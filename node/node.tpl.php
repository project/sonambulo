<?php

/**
 * @file
 * node.tpl.php
 *
 * documentation
 * http://api.drupal.org/api/drupal/modules!node!node.tpl.php/6
 */
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php print ($sticky ? ' sticky' : ''); ?><?php print (!$status ? ' node-unpublished' : ''); ?> clear-block">

<?php print $picture ?>

<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <div class="meta">
  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted ?></span>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="terms terms-inline"><?php print $terms ?></div>
  <?php endif;?>
  </div>

    <?php print $content ?>

  <?php print $links; ?>
</div>
