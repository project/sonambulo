Sonambulo is a base theme for Drupal 6, with clean markup & CSS. 
Less divitis, more fun, and say yes to HTML5 on Drupal 6!!

Like other blank canvas base themes, this theme will NOT make 
your 
site look sexy, but Sonambulo will help you clean up the excessive 
markup that Drupal provides out of the box.

============ Install ============

Since the themes folder at the top level of Drupal is typically 
reserved for Drupal core themes, you should create a sites/all
/themes/ directory and put Somanbulo theme there.

Install as usual, see http://drupal.org/getting-started
/install-contrib/themes for further information. 

============ Features ============

* HTML5's structural elements
* Include html5shiv.j to enable use of HTML5 
sectioning elements 
in legacy Internet Explorer.
* All menus are wrapped in a <nav> - HTML5-style 
- instead of the <div>
* Relative em font sizes
* Rewrite views-view-grid.tpl.php, divs instead 
of table


============ Recommended modules ============

* Style Stripper (http://drupal.org/project
/stylestripper) - Style Stripper lets you turn off 
CSS from modules, including Drupal core.
* Style Guide (http://drupal.org/project/styleguide) 
- Install Style Guide module to preview for common 
HTML elements.
* Semantic Views (http://drupal.org/project
/semanticviews) 
- Clean and semantic Views.
* Semantic CCK (http://drupal.org/project/semantic_cck) 
- customizing the HTML output of CCK fields.

============ To-do ============

* Better documentation
* Better CSS classes to Blocks
* Include forms CSS framework

============ Usage ============

Sonambulo theme can be used as a base theme.

============ Browser Support ============
So far tested on IE7+, Firefox 3.6+, Chrome 
17.0+ and Safari 5+
----------------------------------------
CONTRIBUTORS
----------------------------------------
leivajd@gmail.com @leivajd drp.leivajd.com
