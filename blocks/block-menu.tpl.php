<?php

/**
 * @file
 * block.tpl.php
 *
 * documentation
 * http://api.drupal.org/api/drupal/modules%21system%21block.tpl.php/6
 */
?>
<nav id="block-<?php print $block->module . '-' . $block->delta; ?>" class="block block-<?php print $block->module ?>" role="navigation">
  <?php if ($block->subject): ?>
    <h2 class="block-title"><?php print $block->subject ?></h2>
  <?php endif;?>
  
  <?php print $block->content ?>
</nav>
