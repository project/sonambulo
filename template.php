<?php
/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * Template includes + preprocess
 */

/**
 * Implements hook_theme_breadcrumb().
 */
function sonambulo_breadcrumb($breadcrumb) {

  if (!empty($breadcrumb)) {
    $str = '<ul class="breadcrumb">';
    foreach ($breadcrumb as $key => $crumb) {
      if ($key == 0) {
        $class = 'first';
      }
      elseif ($key == (count($breadcrumb) - 1)) {
        $class = 'last';
      }
      else {
        $class = 'child';
      }
      $str .= '<li class="' . $class . '">' . $crumb . '</li>';
    }
    $str .= '</ul>';
    return $str;
  }
}
