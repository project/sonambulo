<?php

/**
 * @file
 * Displays a single Drupal page.
 *
 * documentation
 * http://api.drupal.org/api/drupal/modules%21system%21page.tpl.php/6
 */
?>
<!DOCTYPE html>
<html lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- Mobile viewport optimized: h5bp.com/viewport -->
<meta name="viewport" content="width=device-width">

<?php print $styles; ?>
  
<!-- Source code http://code.google.com/p/html5shiv/ -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php print $scripts; ?>

<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>

</head>
<body class="<?php print $body_classes; ?>">
  <a href="#content-king" class="offscreen reveal"><?php print t('Skip to main content'); ?></a>
  <header class="masthead" role="banner">
    <div class="group">	
      <?php if (!empty($logo)): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <hgroup>
        <?php if ($is_front || (arg(0) == 'i')):?>
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
        <?php endif; ?>
        <?php else:?>
          <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
        <?php endif; ?>
        
        <?php if (!empty($site_slogan)): ?>
          <h2><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      </hgroup>	
      <?php if (!empty($header)): ?>    
        <?php print $header; ?>
      <?php endif; ?>
    </div>
  </header> 
  <!-- /header -->
  
  <div id="content-king" class="main-content" role="main">
    <div class="group">
      <div class="content">
        <?php if (!empty($breadcrumb)): ?>
          <?php print $breadcrumb; ?>
        <?php endif; ?>
        
        <?php if ((!$is_front) & (!empty($title))): ?><h1 class="page-title"><?php print $title; ?></h1><?php endif; ?>
        
        <?php if (!empty($tabs)): ?>
          <?php print $tabs; ?>
        <?php endif; ?>

        <?php if (!empty($messages)): print $messages; endif; ?>
        <?php if (!empty($help)): print $help; endif; ?>

        <?php print $content; ?>
      </div> 
      <!-- /content -->

      <?php if (!empty($left)): ?>
        <aside class="col-left" role="complementary">
          <?php print $left; ?>
        </aside> 
        <!-- /sidebar-left -->
      <?php endif; ?>
      
      <?php if (!empty($right)): ?>
        <aside class="col-right" role="complementary">
          <?php print $right; ?>
        </aside> 
        <!-- /sidebar-right -->
      <?php endif; ?>
    </div>
  </div> 
  <!-- /main-content -->
  
  <footer class="footer" role="contentinfo">
    <div class="group">
      <?php print $footer_message; ?>
      <?php if (!empty($footer)): print $footer; endif; ?>
    </div>
  </footer> 
  <!-- /footer -->
  
  <?php print $closure; ?>
</body>
</html>
