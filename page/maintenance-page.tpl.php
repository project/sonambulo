<?php

/**
 * @file
 * Displays a single Drupal page.
 *
 * documentation:
 * http://api.drupal.org/api/drupal/modules%21system%21page.tpl.php/6
 */
?>
<!DOCTYPE html>
<html lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<?php print $styles; ?>

<!-- Source code http://code.google.com/p/html5shiv/ -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php print $scripts; ?>

<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>

</head>
<body class="<?php print $body_classes; ?>">
  <header class="masthead" role="banner">
    <div class="row">	
      <?php if (!empty($logo)): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
        <?php endif; ?>
    </div>
  </header> <!-- /header -->

  <div class="main-content" role="main">
    <div class="row">
      <div class="content">
        <h1><?php print $title; ?></h1>
        <h2><?php print $site_name; ?> </h2>
        <?php print $site_slogan; ?>
        <?php print $content; ?>
      </div>
    </div> <!-- /container -->
<?php print $closure; ?>
</body>
</html>
