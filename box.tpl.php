<?php

/**
 * @file
 * box.tpl.php
 *
 * Theme implementation to display a box
 */
?>
<?php if ($title): ?>
  <h2><?php print $title ?></h2>
<?php endif; ?>
<?php print $content ?>
